<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserPoints extends Model
{
    protected $fillable = ['user_id', 'point', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
