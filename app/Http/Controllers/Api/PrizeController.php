<?php

namespace App\Http\Controllers\Api;

use App\Model\AdminSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrizeController extends Controller
{
    //
    /*public function listPrize(){
        $data = ['success' => false, 'data' => [], 'message' => __('Something went wrong')];

        $settings = AdminSetting::orderBy('id', 'ASC')->get();

        $item = [];
        $daily = [];
        $weekly = [];
        if (isset($settings)) {
            foreach ($settings as $list) {
                if( in_array($list->slug, array('first_prize_day','second_prize_day','third_prize_day') ) )
                {
                    $daily[$list->slug] = $list->value;

                }
                if( in_array($list->slug, array('first_prize_week','second_prize_week','third_prize_week') ) )
                {
                    $weekly[$list->slug] = $list->value;
                }

            }

            if (!empty($item)) {
                $data['message'] = __('Prize List');
                $data['success'] = true;
                $data['daily'] = $daily;
                $data['weekly'] = $weekly;
            }
        } else {
            $data ['success'] =  false;
            $data['message'] = __('No data found');
        }
        return response()->json($data);
    }*/

    public function listPrize(){
        $data = ['success' => false, 'data' => [], 'message' => __('Something went wrong')];

        $settings = AdminSetting::orderBy('id', 'ASC')->get();

        $item = [];
        if (isset($settings)) {
            foreach ($settings as $list) {
                if( in_array($list->slug, array('first_prize_day','second_prize_day','third_prize_day','first_prize_week','second_prize_week','third_prize_week') ) )
                {
                    $item[] = [
                        'id' => $list->id,
                        $list->slug => $list->value,
                    ];
                }

            }

            if (!empty($item)) {
                $data['message'] = __('Prize List');
                $data['success'] = true;
                $data['prize_list'] = $item;
            }
        } else {
            $data ['success'] =  false;
            $data['message'] = __('No data found');
        }
        return response()->json($data);
    }
}
