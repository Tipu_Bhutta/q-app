<?php

namespace App\Http\Controllers\Api;

use App\Services\CommonService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\Model\UserAnswer;
use App\Model\UserCoin;
use Illuminate\Support\Facades\Auth;

class CoinController extends Controller
{
    //deduct user coin
    public function deductCoin(Request $request)
    {
        $data = ['success' => false, 'message' => __('Something Went wrong.')];
        $rules=[
            'coin' => 'required|numeric',
        ];
        $messages = [
            'coin.required' => 'The Coin field can not empty'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = [];
            $e = $validator->errors()->all();
            foreach ($e as $error) {
                $errors[] = $error;
            }
            $response = ['success' => false, 'message' => $errors];

            return response()->json($response);
        }
        $type= 1;
        $response = app(CommonService::class)->addOrDeductCoin($request->coin, $type);

        if (isset($response['status']) && isset($response['message'])) {
            $data['success'] = $response['status'];
            $data['available_coin'] = $response['available_coin'];
            $data['message'] = $response['message'];
        }

        return response()->json($data);

    }

    //earn coin process
    public function earnCoin(Request $request)
    {
        $data = ['success' => false, 'message' => __('Something Went wrong.')];
        $rules=[
            'coin' => 'required|numeric',
        ];
        $messages = [
            'coin.required' => 'The Coin field can not empty'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = [];
            $e = $validator->errors()->all();
            foreach ($e as $error) {
                $errors[] = $error;
            }
            $response = ['success' => false, 'message' => $errors];

            return response()->json($response);
        }
        $type= 2;
        $response = app(CommonService::class)->addOrDeductCoin($request->coin, $type);

        if (isset($response['status']) && isset($response['message'])) {
            $data['success'] = $response['status'];
            $data['message'] = $response['message'];
            $data['available_coin'] = $response['available_coin'];
        }

        return response()->json($data);

    }


    // daily earn coin

    public function earnCoinDaily(Request $request)
    {
        $data = ['success' => false, 'message' => __('Something Went wrong.')];
        $rules=[
            'type' => 'required|numeric',
        ];
        $messages = [
            'type.required' => 'The type field can not empty'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = [];
            $e = $validator->errors()->all();
            foreach ($e as $error) {
                $errors[] = $error;
            }
            $response = ['success' => false, 'message' => $errors];

            return response()->json($response);
        }

        if( $request->type == 1 )
        {
            $todayCheck = User::where('id',Auth::user()->id)->whereDate('daily_date',Carbon::today())->count();
            info("check data ===".print_r($todayCheck,true));
            if( $todayCheck != 0 ){

                $response = ['success' => false, 'message' => "You have already avail the today points"];
                return response()->json($response);
            }

            $todayCheck = UserAnswer::where('user_id',Auth::user()->id)->whereDate('created_at',Carbon::today())->where('is_correct',1)->orderby('id','desc')->first();
            info("check data ===".json_encode($todayCheck));

            if( $todayCheck != null ){
                $todayCheck->point = ($todayCheck->point) + ($request->point);
                $todayCheck->save();
//                return;
            }
            else{
                if( $todayCheck == null )
                {
                    $answer = new UserAnswer();
                    $answer->user_id = Auth::user()->id;
                    $answer->category_id = 1;
                    $answer->question_id = 50;
                    $answer->is_correct = 1;
                    $answer->point = $request->point;
                    $answer->save();
                }
            }
            $todayCheck = User::where('id',Auth::user()->id)->first();
            $todayCheck->daily_date = Carbon::now()->toDateString();
            $todayCheck->save();


            $available_point = UserAnswer::where('user_id',Auth::user()->id)->sum('point');
            $available_coin = UserCoin::where('user_id',Auth::user()->id)->sum('coin');

            $response = ['success' => true, 'message' => "success","available_point"=>$available_point,"available_coin"=>$available_coin];
            return response()->json($response);
        }
        else{

            if( $request->has('point') && $request->point > 0 )
            {
                $todayCheck = UserAnswer::where('user_id',Auth::user()->id)->whereDate('created_at',Carbon::today())->where('is_correct',1)->orderby('id','desc')->first();


                info("check data type 2 ===".print_r($todayCheck,true));
                if( $todayCheck != null ){
                    $todayCheck->point = ($todayCheck->point) + ($request->point);
                    $todayCheck->save();

                }
                else{

                        $answer = new UserAnswer();
                        $answer->user_id = Auth::user()->id;
                        $answer->category_id = 1;
                        $answer->question_id = 50;
                        $answer->is_correct = 1;
                        $answer->point = $request->point;
                        $answer->save();

                }


                $available_point = UserAnswer::where('user_id',Auth::user()->id)->sum('point');
                $available_coin = UserCoin::where('user_id',Auth::user()->id)->sum('coin');

                $response = ['success' => true, 'message' => "success","available_point"=>$available_point,"available_coin"=>$available_coin];
                return response()->json($response);
            }

            if( $request->has('coin') && $request->coin > 0 )
            {
                $todayCheck = UserCoin::where('user_id',Auth::user()->id)->first();
                info("check data ===".print_r($todayCheck,true));
                if( $todayCheck != null ){
                    $todayCheck->coin = ($todayCheck->coin) + ($request->coin);
                    $todayCheck->save();

                    $available_point = UserAnswer::where('user_id',Auth::user()->id)->sum('point');
                    $available_coin = UserCoin::where('user_id',Auth::user()->id)->sum('coin');

                    $response = ['success' => true, 'message' => "success","available_point"=>$available_point,"available_coin"=>$available_coin];
                    return response()->json($response);
                }

            }
        }

    }
}
