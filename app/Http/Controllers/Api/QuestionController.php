<?php

namespace App\Http\Controllers\Api;

use App\Model\Category;
use App\Model\AdminSetting;
use App\Model\Question;
use App\Model\QuestionOption;
use App\Model\UserAnswer;
use App\Model\UserCoin;
use App\Prize;
use App\User;
use App\lim;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    /*
     * questionCategory
     *
     * Question category list
     *
     *
     *
     *
     */
    public function questionCategory()
    {
        info('category/api call');

        $data = ['success' => false, 'data' => [], 'message' => __('Something went wrong')];



//        $this->CronTaskFunction();
        $categories = Category::where('status', STATUS_ACTIVE)->orderBy('serial', 'ASC')->whereNull('parent_id')->get();
        $data['user_available_coin'] = 0;
        $data['user_available_point'] = calculate_score( Auth::user()->id);
        if (isset(Auth::user()->userCoin->coin)) {
            $data['user_available_coin'] = Auth::user()->userCoin->coin;
        }
        $item = [];
        if (isset($categories)) {
            foreach ($categories as $list) {

                $getQuiz = AdminSetting::where('slug','quiz_count')->first();
                $totalQuestion = 2;
                if( $getQuiz !=null)
                {
                    $totalQuestion = $getQuiz->value;
                }


                $items = lim::orderBy('id', 'DESC')->first();
                $questionlim = $items->questionlim;

                $todayDate = date('Y-m-d');
                $countAttemptedAns= DB::table('user_answers')
                    ->where('user_id', Auth::user()->id)
                    ->where('question_id','!=',50)
                    ->where('created_at', 'like', $todayDate.'%')
                    ->count();

                info("Quiz limit Question====".$totalQuestion);

                info("count answer====".$countAttemptedAns);

                $countLimit=($countAttemptedAns/10);

                info("count limit before====".$countLimit);

                $countLimit=ceil($countLimit);

                info("count limit after====".$countLimit);

                $remain_limit = $questionlim-$countLimit;
                if( $remain_limit < 0 )
                {
                    $remain_limit = 0;
                }

                // if($questionlim<=$countLimit){
                //     $data = [
                //         'success' => false,
                //         'message' => __('Sorry, You Can Play only '.$questionlim.' Quizes / day'),
                //         'countLimit'=> $countLimit,
                //         'questionlim'=> $questionlim
                //     ];

                //     return response()->json($data);

                // }


                info("setting admob_coin ".allsetting('admob_coin'));

                if(!empty(allsetting('admob_coin'))) {
                    $admob_coin = allsetting('admob_coin');
                }

                info("setting applovin_coins ".allsetting('applovin_coins'));

                if(!empty(allsetting('applovin_coins'))) {
                    $applovien_coin = allsetting('applovin_coins');
                }

                info("setting applovin_points ".allsetting('applovin_points'));

                if(!empty(allsetting('applovin_points'))) {
                    $applovien_point = allsetting('applovin_points');
                }

                info("setting admob_points ".allsetting('admob_points'));
                if(!empty(allsetting('admob_points'))) {
                    $admob_point = allsetting('admob_points');
                }


                $item[] = [
                    'id' => $list->id,
                    'category_id' => encrypt($list->id),
                    'name' => $list->name,
                    'description' => $list->description,
                    'image' => !empty($list->image) ? asset(path_category_image() . $list->image) : "",
                    'qs_limit' => $totalQuestion,
                    'time_limit' => $list->time_limit,
                    'max_limit' => $list->max_limit,
                    'serial' => $list->serial,
                    'status' => $list->status,
                    'coin' => $list->coin,
                    'total_quiz_of_day' => $questionlim,
                    'remaining_quiz_of_day' => $remain_limit,
                    'sub_category' => $list->count_sub_category->count(),
                    'question_amount' => count_question($list->id),
                    'is_locked' => check_category_unlock($list->id,$list->coin),
                    'admob_coin' =>$admob_coin,
                    'applovin_coin' =>$applovien_coin,
                    'applovin_points' =>$applovien_point,
                    'admob_points' =>$admob_point
                ];
            }

            if (!empty($item)) {
                $data['message'] = __('Category List');
                $data['success'] = true;
                $data['category_list'] = $item;
            }
        } else {
            $data ['success'] =  false;
            $data['message'] = __('No data found');
        }
        return response()->json($data);
    }

    // public function questionCategory()
    // {
    //     info('category/api call');

    //     $data = ['success' => false, 'data' => [], 'message' => __('Something went wrong')];

    //     $categories = Category::where('status', STATUS_ACTIVE)->orderBy('serial', 'ASC')->whereNull('parent_id')->get();
    //     $data['user_available_coin'] = 0;
    //     $data['user_available_point'] = calculate_score( Auth::user()->id);
    //     if (isset(Auth::user()->userCoin->coin)) {
    //         $data['user_available_coin'] = Auth::user()->userCoin->coin;
    //     }
    //     $item = [];
    //     if (isset($categories)) {
    //         foreach ($categories as $list) {

    //             $attemptedQuestion = UserAnswer::where('user_id',Auth::user()->id)->whereDate('created_at',Carbon::today())->count();
    //             // dd($attemptedQuestion);
    //             $getQuiz = AdminSetting::where('slug','quiz_count')->first();
    //             //$totalQuestion = 2;
    //             if( $getQuiz !=null)
    //             {
    //                 $totalQuestion = $getQuiz->value;
    //             }




    //              $items = lim::orderBy('id', 'DESC')->first();
    //             $questionlim = $items->questionlim;

    //              $remainingQuestion = $questionlim - $attemptedQuestion;

    //             // $remainingQuestion = $attemptedQuestion;

    //              if( $remainingQuestion < 0 )
    //              {
    //                  $remainingQuestion = 0;
    //              }
    //             // if( $attemptedQuestion >  $questionlim )
    //             //   $remainingQuestion = $questionlim;

    //             $item[] = [
    //                 'id' => $list->id,
    //                 'category_id' => encrypt($list->id),
    //                 'name' => $list->name,
    //                 'description' => $list->description,
    //                 'image' => !empty($list->image) ? asset(path_category_image() . $list->image) : "",
    //                 'qs_limit' => $questionlim,
    //                 'time_limit' => $list->time_limit,
    //                 'max_limit' => $list->max_limit,
    //                 'serial' => $list->serial,
    //                 'status' => $list->status,
    //                 'coin' => $list->coin,
    //                 'total_quiz_of_day' => $totalQuestion,
    //                 'remaining_quiz_of_day' => $remainingQuestion,
    //                 'sub_category' => $list->count_sub_category->count(),
    //                 'question_amount' => count_question($list->id),
    //                 'is_locked' => check_category_unlock($list->id,$list->coin)
    //             ];
    //         }

    //         if (!empty($item)) {
    //             $data['message'] = __('Category List');
    //             $data['success'] = true;
    //             $data['category_list'] = $item;
    //         }
    //     } else {
    //         $data ['success'] =  false;
    //         $data['message'] = __('No data found');
    //     }
    //     return response()->json($data);
    // }

    /*
     * questionSubCategory
     *
     * Question sub category list
     *
     *
     *
     *
     */
    public function questionSubCategory($id)
    {

        info("questionSubCategory");
        $data = ['success' => false, 'data' => [], 'message' => __('Something went wrong')];
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            $data = [
                'success' => false,
                'message' => __('Invalid Category id')
            ];

            return response()->json($data);
        }
        $category = Category::findOrFail($id);
        $categories = Category::where('status', STATUS_ACTIVE)->orderBy('serial', 'ASC')->where(['parent_id'=>$id])->get();
        $data['user_available_coin'] = 0;
        $data['user_available_point'] = calculate_score( Auth::user()->id);
        $data['parent_category_name'] = $category->name;
        if (isset(Auth::user()->userCoin->coin)) {
            $data['user_available_coin'] = Auth::user()->userCoin->coin;
        }
        $item = [];
        if (isset($categories)) {
            foreach ($categories as $list) {
                $item[] = [
                    'id' => $list->id,
                    'sub_category_id' => encrypt($list->id),
                    'name' => $list->name,
                    'description' => $list->description,
                    'image' => !empty($list->image) ? asset(path_category_image() . $list->image) : "",
                    'qs_limit' => $list->qs_limit,
                    'time_limit' => $list->time_limit,
                    'max_limit' => $list->max_limit,
                    'serial' => $list->serial,
                    'status' => $list->status,
                    'coin' => $list->coin,
                    'question_amount' => count_question($list->id),
                    'is_locked' => check_category_unlock($list->id,$list->coin)
                ];
            }

            if (!empty($item)) {
                $data['message'] = __('Category List');
                $data['success'] = true;
                $data['sub_category_list'] = $item;
            }
        } else {
            $data ['success'] =  false;
            $data['message'] = __('No data found');
        }
        return response()->json($data);
    }

    /*
     * singleCategory
     *
     * Show the Question list under this category
     *
     *
     *
     *
     */

    public function singleCategoryQuestion($type,$id,$user_id)
    {

        info("singleCategoryQuestion");

        $todayDate = date('Y-m-d');
        $questionlim = DB::table('lims')->select('questionlim')->where('id', '=', '2')->value('questionlim');
        $qs_limit = DB::table('categories')->select('qs_limit')->where('id', '=', '1')->value('qs_limit');
        $countAttemptedAns= DB::table('user_answers')->where('user_id', '=', $user_id)->where('created_at', 'like', $todayDate.'%')->count();
        $countLimit=($countAttemptedAns/$qs_limit);

        $countLimit=floor($countLimit);

        /*
         $data = [
                 'success' => false,
                 'QLimit'=>   $questionlim,
                 'countAttemp'=>$countAttempted,
                 'userId'=>$user_id,
                 'message' => __('Limit Reached')
             ];

             return response()->json($data);

            */

        /*  if($countAttempted>=$questionlim){*/
        if($questionlim<=$countLimit){
            $data = [
                'success' => false,
                'message' => __('Sorry, You Can Play only '.$questionlim.' Quizes / day'),
                'countLimit'=> $countLimit,
                'questionlim'=> $questionlim
            ];

            return response()->json($data);

        }

        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            $data = [
                'success' => false,
                'message' => __('Invalid Category id')
            ];

            return response()->json($data);
        }
        $data = ['success' => false, 'data' => [], 'message' => __('Something went wrong')];
        $data['user_available_coin'] = 0;
        $data['user_available_point'] = calculate_score( Auth::user()->id);;
        if (isset(Auth::user()->userCoin->coin)) {
            $data['user_available_coin'] = Auth::user()->userCoin->coin;
        }
        $category = Category::where('id',$id)->first();
        $limit = $category->qs_limit;
        $timeLimit = $category->time_limit;
        $availableQuestions = '';

        if($type ==1) {
            $availableQuestions = Question::with('question_option')
                ->where(['questions.category_id' => $id,'questions.status'=> STATUS_ACTIVE])
                /*->whereNotIn('questions.id', UserAnswer::select('question_id')->where(['user_id' => Auth::id()]))*/
                ->select('questions.*')
                ->inRandomOrder()
                ->limit($limit)
                ->get();
        } else {
            $availableQuestions = Question::with('question_option')
                ->where(['questions.sub_category_id' => $id,'questions.status'=> STATUS_ACTIVE])
                /*->whereNotIn('questions.id', UserAnswer::select('question_id')->where(['user_id' => Auth::id()]))*/
                ->select('questions.*')
                ->inRandomOrder()
                ->limit($limit)
                ->get();
        }

        $data['hints_coin'] = 0;
        if (!empty(allsetting('hints_coin'))) {
            $data['hints_coin'] = allsetting('hints_coin');
        }
        $lists = [];

        if (isset($availableQuestions)) {
            $totalQuestion = 0;
            $totalCoin = 0;
            $totalPoint = 0;
            foreach ($availableQuestions as $question) {
                $itemImage = [];
                if(isset($question->question_option[0])) {
                    $itemImage [] = [
                        'id' => isset($question->question_option[0]) ? $question->question_option[0]->id : '',
                        'question_option' => isset($question->question_option[0]) && (!empty($question->question_option[0]->option_image)) ?  asset(path_question_option1_image() . $question->question_option[0]->option_image) : $question->question_option[0]->option_title,
                        'type' => isset($question->question_option[0]) && (!empty($question->question_option[0]->option_image)) ? 1 : 0
                    ];
                }
                if(isset($question->question_option[1])) {
                    $itemImage [] = [
                        'id' => isset($question->question_option[1]) ? $question->question_option[1]->id : '',
                        'question_option' => isset($question->question_option[1]) && (!empty($question->question_option[1]->option_image)) ?  asset(path_question_option2_image() . $question->question_option[1]->option_image) : $question->question_option[1]->option_title,
                        'type' => isset($question->question_option[1]) && (!empty($question->question_option[1]->option_image)) ? 1 : 0
                    ];
                }


                if(isset($question->question_option[2])) {
                    $itemImage [] = [
                        'id' => isset($question->question_option[2]) ? $question->question_option[2]->id : '',
                        'question_option' => isset($question->question_option[2]) && (!empty($question->question_option[2]->option_image)) ? asset(path_question_option3_image() . $question->question_option[2]->option_image) : $question->question_option[2]->option_title,
                        'type' => isset($question->question_option[2]) && (!empty($question->question_option[2]->option_image)) ? 1 : 0
                    ];
                }
                if(isset($question->question_option[3])) {
                    $itemImage [] = [
                        'id' => isset($question->question_option[3])  ? $question->question_option[3]->id : '',
                        'question_option' => isset($question->question_option[3]) && (!empty($question->question_option[3]->option_image)) ? asset(path_question_option4_image() . $question->question_option[3]->option_image) : $question->question_option[3]->option_title,
                        'type' => isset($question->question_option[3]) && (!empty($question->question_option[3]->option_image)) ? 1 : 0
                    ];
                }
                if(isset($question->question_option[4])) {
                    $itemImage [] = [
                        'id' => isset($question->question_option[4]) ? $question->question_option[4]->id : '',
                        'question_option' => isset($question->question_option[4]) && (!empty($question->question_option[4]->option_image)) ? asset(path_question_option5_image() . $question->question_option[4]->option_image) : $question->question_option[4]->option_title,
                        'type' => isset($question->question_option[4]) && (!empty($question->question_option[4]->option_image)) ? 1 : 0
                    ];
                }

                $lists[] = [
                    'category' => $question->qsCategory->name,
                    'sub_category' => isset($question->qsSubCategory->name) ? $question->qsSubCategory->name : '',
                    'category_id' => $question->qsCategory->id,
                    'sub_category_id' => isset($question->qsSubCategory->id) ? $question->qsSubCategory->id : '',
                    'id' => $question->id,
                    'question_id' => encrypt($question->id),
                    'title' => $question->title,
                    'has_image' => !empty($question->image) ? 1 : 0,
                    'image' => !empty($question->image) ? asset(path_question_image() . $question->image) : "",
                    'point' => $question->point,
                    'coin' => $question->coin,
                    'time_limit' => isset($question->time_limit) ? $question->time_limit : $timeLimit,
                    'status' => $question->status,
                    'hints' => $question->hints,
                    'skip_coin' => $question->skip_coin,
                    'option_type' => $question->type,
                    'options' => $itemImage,
//                    'options2' => $itemImage,
//                    'options' => $question->question_option->toArray()
                ];

                $totalQuestion ++;
                $totalPoint = $totalPoint + $question->point;
                $totalCoin = $totalCoin + $question->coin;
            }


            if (!empty($lists)) {
                $data['success'] = true;
                $data['totalQuestion'] = $totalQuestion;
                $data['totalPoint'] = $totalPoint;
                $data['totalCoin'] = $totalCoin;
                $data['availableQuestionList'] = $lists;
                $data['message'] = __('Available Question List');

                $data['countLimit']= $countLimit;
                $data['questionlim']= $questionlim;
            } else {
                $data = [
                    'success' => false,
                    'message' => __('No question found.')
                ];
            }
        } else {
            $data = [
                'success' => false,
                'message' => __('No question found.')
            ];
        }

        return response()->json($data);
    }


    /*
     * submitAnswer
     *
     * Submit the answer
     *
     *
     *
     *
     */
    public function submitAnswer(Request $request, $id)
    {


        $data = ['success' => false, 'data' => [], 'message' => __('Something Went wrong !')];
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            $data = [
                'success' => false,
                'message' => __('Invalid Question id')
            ];

            return response()->json($data);
        }


        info("submitAnswer-===============".$id);
        info("Request data ==========".print_r($request->all(),true));

        $validator = Validator::make($request->all(), [
            'answer' => 'required',
//            'time_limit' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = [];
            $e = $validator->errors()->all();
            foreach ($e as $error) {
                $errors[] = $error;
            }
            $data['message'] = $errors;

            return response()->json($data);
        }
        try {
            $rightAnswer = [];
            $userCoins = UserCoin::where('user_id', Auth::user()->id)->first();
            if(empty($userCoins)) {
                return response()->json([
                    'success' => false,
                    'message' => 'User coin account not found.',
                ]);
            }

            info("correct Answrer-===============".$id);
            $correctAnswer = QuestionOption::where(['question_id'=> $id, 'is_answer' => ANSWER_TRUE])->first();
            if(isset($correctAnswer)) {
                $rightAnswer = [
                    'option_id' => $correctAnswer->id,
//                    'option_title' => $correctAnswer->option_title,
                ];
            }
            $question = Question::where(['id' => $id])->first();
            $option = QuestionOption::where(['id'=> $request->answer, 'question_id'=> $id])->first();
            $userAnswer = UserAnswer::where(['question_id' => $id, 'user_id' => Auth::user()->id])->first();

            $input =[
                'user_id' => Auth::user()->id,
                'category_id' => $question->qsCategory->id,
                'question_id' => $question->id,
                'type' => $question->type,
            ];
            if ($option) {

//                $viewTime = Carbon::parse($userAnswer->created_at);
//                $checkTime = Carbon::parse(Carbon::now());
//                $diffTime = $checkTime->diffInSeconds($viewTime);
//                //dd($sendTime,$checkTime, $diffTime);
//                if ($diffTime <= (60 * $request->time_limit)) {
                if ($option->is_answer == ANSWER_TRUE) {
                    $input['is_correct'] = ANSWER_TRUE;
                    $input['point'] = $question->point;
                    $input['coin'] = $question->coin;
                    if (empty($userAnswer)) {
                        $updatePoint = $userCoins->increment('coin', $question->coin);
                    }
                    $data = [
                        'success' => true,
                        'message' => __('Right Answer'),
                    ];
                } else {
                    $data = [
                        'success' => false,
                        'message' => __('Wrong Answer'),
                        'right_answer' => $rightAnswer
                    ];
                }
//                } else {
//                    $data = [
//                        'success' => false,
//                        'message' => __('Sorry Time out!')
//                    ];
//                }
            } else {
                $data = [
                    'success' => false,
                    'message' => __('Wrong Answer'),
                    'right_answer' => $rightAnswer
                ];
            }
            if ($userAnswer) {
                //$userAnswer->update($input);
                $insert = UserAnswer::create($input);
            } else {
                $insert = UserAnswer::create($input);
            }

            $data['total_point'] = calculate_score( Auth::user()->id);
            $data['total_coin'] = User::where('id',Auth::user()->id)->first()->userCoin->coin;

        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
//                'message' => 'Something went wrong. Please try again!',
            ]);
        }

        return response()->json($data);
    }

    /*
     * leaderBoard
     *
     * Leader board who have attend the quiz
     * And show their score and ranking
     *
     *
     *
     */
    public function leaderBoard($type=null)
    {

        info('leaderBoard'.$type);
        $data = ['success' => false, 'data' => [], 'message' => __('Something Went wrong !')];

        $this->CronTaskFunction();
        if ($type == 2) {
            $leaders = UserAnswer::select(
                DB::raw('SUM(point) as score, user_id'))
                ->groupBy('user_id')
                ->whereDate('created_at', Carbon::today())
                ->orderBy('score', 'DESC')
                ->get();
        }
        elseif($type == 3)
        {

            $get_adminSetting =  AdminSetting::where(['slug' => 'weekly_points_reset_date'])->first();

            info("admin setting ====".json_encode($get_adminSetting));

            $week_no = $get_adminSetting->value;
            $start_date = Carbon::parse($week_no);
            $end_date = Carbon::parse($week_no);
            $start_date->endOfDay();
            $end_date->subDays(7)->startOfDay();

            info('start date ============='.$start_date);
            info('end==================='.$end_date);
            $leaders = UserAnswer::select(
                DB::raw('SUM(point) as score, user_id'))
                ->groupBy('user_id')
                ->wherebetween('created_at', [$end_date,$start_date])
                ->orderBy('score', 'DESC')
                ->get();

            $get_user_ids = User::where('role',2)->pluck('id');

            $c = collect();
            foreach ( $get_user_ids as $single )
            {

                $local = new UserAnswer();
                $local->user_id = $single;
                $local->score = 0;
                if( $leaders->isNotEmpty())
                {
                    foreach ($leaders as $leader)
                    {
                        if( $leader->user_id == $single )
                        {
                            $local->score = $leader->score;
                            break;
                        }
                    }
                }

                $c->push($local);
            }

            $sorted = $c->sortByDesc('score');

            $leaders = $sorted;

        }
        else {


            $leadersPrizes = Prize::select(
                DB::raw('SUM(points) as score, user_id'))
                ->groupBy('user_id')
                ->orderBy('score', 'DESC')
                ->get();



//            info("all monthly  points======".json_encode($leadersPrizes));

            $get_adminSetting =  AdminSetting::where(['slug' => 'weekly_points_reset_date'])->first();

            info("admin setting ====".json_encode($get_adminSetting));

            $week_no = $get_adminSetting->value;
            $start_date = Carbon::parse($week_no);
            $end_date = Carbon::parse($week_no);
            $start_date->endOfDay();
            $end_date->subDays(7)->startOfDay();

//            info('start date ============='.$start_date);
//            info('end==================='.$end_date);
            $weeklyLeaders = UserAnswer::select(
                DB::raw('SUM(point) as score, user_id'))
                ->groupBy('user_id')
                ->wherebetween('created_at', [$end_date,$start_date])
                ->orderBy('score', 'DESC')
                ->having('score','>',0)
                ->get();

//            $leaders = $leadersPrizes;
//            info("weekly points=========".json_encode($weeklyLeaders));
//            info("weekly points========= ============================end");
            $allCollection = array();

            if( count($leadersPrizes) != 0 &&  count($weeklyLeaders) !=0)
            {
                $allCollection = array_merge($leadersPrizes->toArray(),$weeklyLeaders->toArray());
            }
            elseif( count($weeklyLeaders) !=0 )
            {
                $allCollection = $weeklyLeaders->toArray();
            }
            elseif( count($leadersPrizes) != 0 )
            {
                $allCollection = $leadersPrizes->toArray();
            }

//            $leadersPrizes
//            $allCollection = collect($allCollection)->sortByDesc('score')->values();
//            info("all collection point=======".json_encode($allCollection));
//            $allCollection->mapWithKeys(function ($item, $key) {
//                return [$item['user_id'] => $item['score']];
//            });


//            $allCollection = $allCollection->sum('score');
//                info("all collection point after merge =======".json_encode($allCollection));
            $user_array = array();
            $new_user_array = collect();
            if( count( $allCollection ) != 0 )
            {
                foreach( $allCollection as $single )
                {

                    if( ! in_array($single['user_id'],$user_array) )
                    {
                        $local = new UserAnswer();
                        $local->user_id = $single['user_id'];
                        $local->score = $single['score'];

                        $user_array[] = $single['user_id'];

                        $new_user_array->push($local);
//                        $new_user_array[] = $local;
                    }
                    else
                    {
                        if( count($new_user_array) != 0)
                        {
                            for($i=0, $iMax = count($new_user_array); $i < $iMax; $i++)
                            {
                                if( $new_user_array[$i]->user_id == $single['user_id']  )
                                {
                                    $new_user_array[$i]->score += $single['score'];
                                    break;
                                }
                            }
                        }
                    }

                }
            }

            $leaders = $new_user_array->sortByDesc('score');
            info("all collection point=======".json_encode($leaders));
//            $leaders = collect($leaders);

//
//
//
//            info("Leader board==".json_encode($leaders));
        }

        $lists = [];
        if (isset($leaders)) {
            $rank = 1;
            foreach ($leaders as $item) {

                $lists[] = [
                    'user_id' => $item->user_id,
                    'photo' => asset(pathUserImage() . $item->user->photo),
                    'name' => $item->user->name,
                    'score' => $item->score,
                    'coin' => isset($item->user->userCoin->coin) ? $item->user->userCoin->coin : 0,
                    'ranking' => $rank++,
                ];
            }
            if (!empty($lists)) {
                $data = [
                    'success' => true,
                    'leaderList' => $lists,
                ];
            } else {
                $data = [
                    'success' => false,
                    'message' => __('No data found')
                ];
            }
        } else {
            $data = [
                'success' => false,
                'message' => __('No data found')
            ];
        }

        return response()->json($data);
    }

    public function qlimList()
    {
        $items = lim::orderBy('id', 'DESC')->get();
        $questionlim = $items[0]->questionlim;
        $data = [
            'success' => true,
            'items' => $items,
            'questionlim' => $questionlim,
        ];
        return response()->json($data);

    }

    public  function CronTaskFunction(){

        info("Dashboard Leader board");

        $get_adminSetting =  AdminSetting::where(['slug' => 'weekly_points_reset'])->first();

        $week_no = $get_adminSetting->value;

        $start_week = now()->startOfWeek();

//        info("start of year ".$start_week);

        for ($i = 1; $i <= 7; $i++) {

            if( $week_no == $i )
            {
                $get_admin_date =  AdminSetting::where(['slug' => 'weekly_points_reset_date'])->first();
                $week_date = $get_admin_date->value;
//                $week_date = $start_week;

                info("week Date ================ ".$week_date);
                info("current  Date ==============". now()->toDateString());

                if(    $week_date  < now()->toDateString() )
                {
                    info("Cron runing date && time now");

                    $start_date = Carbon::parse($week_date);

                    $end_date = Carbon::parse($week_date);

                    $end_date = $end_date->subdays(7);


                    info('start date ===============' . $start_date->endOfDay());
                    info('end date===========' . $end_date->startOfDay());
//

                    $leaders = UserAnswer::select(
                        DB::raw('SUM(point) as score, user_id'))
                        ->wherebetween('created_at',[$end_date->startOfDay(), $start_date->endOfDay()])
                        ->groupBy('user_id')
                        ->orderBy('score', 'DESC')
                        ->having('score' ,'>', 0)
                        ->get();

                    if( $leaders->isNotEmpty())
                    {
                        foreach ($leaders as $leader)
                        {
                            info("user_id============".$leader->user_id.'=====score==========='.$leader->score);
                            $prize = new Prize();
                            $prize->points = $leader->score;
                            $prize->user_id = $leader->user_id;
                            $prize->reset_points_date = $start_date->toDateString();
                            $prize->month_date = now()->toDateString();
                            $prize->save();
                        }
                    }

                    UserAnswer::wherebetween('created_at',[$end_date->startOfDay(), $start_date->endOfDay()])
//                    UserAnswer::where('created_at', '>=', $end_date->toDateString() . ' 00:00:00')->where('created_at', '<=', $start_date->toDateString() . ' 23:59:59')
                        ->update(['point' => 0]);
//                        ->get();

//                    info("get data -----------".json_encode($leaders));

                    $next_date = $start_date->addDays(7);
                    info('start date after ' . $next_date);

                    AdminSetting::updateOrCreate(['slug' => 'weekly_points_reset_date'],['value' =>$next_date->toDateString()]);
                }
                else
                {
                    info("Cron Not runing date ");
                }

            }
            $start_week->addDay();
        }
    }




}
