<?php

namespace App\Http\Controllers\Api;

use App\Services\CommonService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PointController extends Controller
{


    //earn point process
    public function earnPoint(Request $request)
    {
        $data = ['success' => false, 'message' => __('Something Went wrong.')];
        $rules=[
            'point' => 'required|numeric',
        ];
        $messages = [
            'point.required' => 'The point field can not empty'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = [];
            $e = $validator->errors()->all();
            foreach ($e as $error) {
                $errors[] = $error;
            }
            $response = ['success' => false, 'message' => $errors];

            return response()->json($response);
        }
        $type= 2;
        $response = app(CommonService::class)->addOrDeductPoint($request->point, $type);

        if (isset($response['status']) && isset($response['message'])) {
            $data['success'] = $response['status'];
            $data['message'] = $response['message'];
            $data['available_point'] = $response['available_point'];
        }

        return response()->json($response);

    }
}
