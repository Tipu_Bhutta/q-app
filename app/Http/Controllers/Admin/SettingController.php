<?php

namespace App\Http\Controllers\Admin;

use App\Model\AdminSetting;
use App\Prize;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    /*
    * generalSetting
    *
    * general Setting
    *
    *
    *
    *
    */

    public function generalSetting()
    {
        $data['pageTitle'] = __('General Setting');
        $data['menu'] = 'setting';
        $data['adm_setting'] = allsetting();

        return view('admin.setting', $data);
    }

    public function generalSettingForWeekly()
    {
        $data['adm_setting'] = allsetting();

        return view('admin.weekly-points', $data);
    }

    /*
    * saveSettings
    *
    * save admin setting data
    *
    *
    *
    *
    */

    public function saveSettings(Request $request)
    {
//        dd($request->all());
        try {
            if (isset($request->company_name)) {
                AdminSetting::where(['slug' => 'company_name'])->update(['value' => $request->company_name]);
            }
            if (isset($request->lang)) {
                AdminSetting::where(['slug' => 'lang'])->update(['value' => $request->lang]);
            }
            if (isset($request->user_registration)) {
                AdminSetting::where(['slug' => 'user_registration'])->update(['value' => $request->user_registration]);
            }
            if (isset($request->app_title)) {
                AdminSetting::where(['slug' => 'app_title'])->update(['value' => $request->app_title]);
            }
            if (isset($request->primary_email)) {
                AdminSetting::where(['slug' => 'primary_email'])->update(['value' => $request->primary_email]);
            }
            if (isset($request->copyright_text)) {
                AdminSetting::where(['slug' => 'copyright_text'])->update(['value' => $request->copyright_text]);
            }
            if (isset($request->hints_coin)) {
                AdminSetting::updateOrCreate(['slug' => 'hints_coin'],['value' => $request->hints_coin]);
            }
            if (isset($request->admob_coin)) {
                AdminSetting::updateOrCreate(['slug' => 'admob_coin'],['value' => $request->admob_coin]);
            }
            if (isset($request->admob_points)) {
                AdminSetting::updateOrCreate(['slug' => 'admob_points'],['value' => $request->admob_points]);
            }
            if (isset($request->applovin_coins)) {
                AdminSetting::updateOrCreate(['slug' => 'applovin_coins'],['value' => $request->applovin_coins]);
            }
            if (isset($request->applovin_points)) {
                AdminSetting::updateOrCreate(['slug' => 'applovin_points'],['value' => $request->applovin_points]);
            }
            if (isset($request->signup_coin)) {
                AdminSetting::updateOrCreate(['slug' => 'signup_coin'],['value' => $request->signup_coin]);
            }
            if (isset($request->quiz_count)) {
                AdminSetting::updateOrCreate(['slug' => 'quiz_count'],['value' => $request->quiz_count]);
            }
            if (isset($request->privacy_policy)) {
                AdminSetting::updateOrCreate(['slug' => 'privacy_policy'],['value' => $request->privacy_policy]);
            }
            if (isset($request->login_text)) {
                AdminSetting::updateOrCreate(['slug' => 'login_text'],['value' => $request->login_text]);
            }
            if (isset($request->signup_text)) {
                AdminSetting::updateOrCreate(['slug' => 'signup_text'],['value' => $request->signup_text]);
            }
            if (isset($request->terms_conditions)) {
                AdminSetting::updateOrCreate(['slug' => 'terms_conditions'],['value' => $request->terms_conditions]);
            }
            if (isset($request->weekly_points_reset)) {
                AdminSetting::updateOrCreate(['slug' => 'weekly_points_reset'],['value' => $request->weekly_points_reset]);
//                info("weeekj ".now()->startOfWeek());
                AdminSetting::updateOrCreate(['slug' => 'weekly_points_reset_date'],['value' =>Carbon::parse(now()->startOfWeek()->addDay($request->weekly_points_reset-1))->toDateString()]);
            }


            // prizes save
            if (isset($request->first_prize_day)) {
                AdminSetting::updateOrCreate(['slug' => 'first_prize_day'])->update(['value' => $request->first_prize_day]);
            }
            if (isset($request->second_prize_day)) {
                AdminSetting::updateOrCreate(['slug' => 'second_prize_day'])->update(['value' => $request->second_prize_day]);
            }
            if (isset($request->third_prize_day)) {
                AdminSetting::updateOrCreate(['slug' => 'third_prize_day'])->update(['value' => $request->third_prize_day]);
            }

            if (isset($request->first_prize_week)) {
                AdminSetting::updateOrCreate(['slug' => 'first_prize_week'])->update(['value' => $request->first_prize_week]);
            }
            if (isset($request->second_prize_week)) {
                AdminSetting::updateOrCreate(['slug' => 'second_prize_week'])->update(['value' => $request->second_prize_week]);
            }
            if (isset($request->third_prize_week)) {
                AdminSetting::updateOrCreate(['slug' => 'third_prize_week'])->update(['value' => $request->third_prize_week]);
            }


            if (isset($request->logo)) {
//                AdminSetting::updateOrCreate(['slug' => 'logo'], ['value' => uploadthumb($request->logo, path_image(), 'logo_', '', '', allsetting()['logo'])]);
                AdminSetting::updateOrCreate(['slug' => 'logo'], ['value' => fileUpload($request['logo'], path_image(), allsetting()['logo'])]);
            }
            if (isset($request->favicon)) {
                AdminSetting::updateOrCreate(['slug' => 'favicon'], ['value' => fileUpload($request['favicon'], path_image(), allsetting()['favicon'])]);
            }
            if (isset($request->login_logo)) {
                AdminSetting::updateOrCreate(['slug' => 'login_logo'], ['value' => fileUpload($request['login_logo'], path_image(), allsetting()['login_logo'])]);
            }





            return redirect()->back()->with(['success' => __('Updated Successfully')]);
        } catch (\Exception $e) {
//            dd($e->getMessage());
            return redirect()->back()->with(['dismiss' => __('Something went wrong')]);
        }
    }


    public function prizes(){
        $data['pageTitle'] = __('Prizes');
        $data['menu'] = 'prizes';
        $data['adm_setting'] = allsetting();

//        dd($data);

        return view('admin.prizes', $data);
    }


}
