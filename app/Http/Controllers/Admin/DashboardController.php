<?php

namespace App\Http\Controllers\Admin;

use App\Model\AdminSetting;
use App\Model\Category;
use App\Model\Question;
use App\Model\UserAnswer;
use App\Prize;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /*
     * adminDashboardView
     *
     * Basic view of admin dashboard
     *
     *
     *
     *
     */
    public function adminDashboardView()
    {
        $data['pageTitle'] = __('Admin|Dashboard');
        $data['menu'] = 'dashboard';
        $data['totalQuestion'] = 0;
        $data['totalCategory'] = 0;
        $data['totalUser'] = 0;
        $data['totalQuestion'] = Question::where('status', STATUS_ACTIVE)->count();
        $data['totalCategory'] = Category::where('status', STATUS_ACTIVE)->count();
        $data['totalUser'] = User::where(['active_status'=> STATUS_ACTIVE, 'role'=> USER_ROLE_USER])->count();
        $data['categories'] = Category::where('status', STATUS_ACTIVE)->orderBy('id', 'DESC')->limit(4)->get();
        $data['questions'] = Question::where('status', STATUS_ACTIVE)->orderBy('id', 'DESC')->limit(4)->get();
        $data['leaders'] = UserAnswer::select(
            DB::raw('SUM(point) as score, user_id'))
            ->groupBy('user_id')
            ->orderBy('score', 'DESC')
            ->limit(5)
            ->get();
        $monthlyUsers = UserAnswer::select(DB::raw('count(DISTINCT user_id) as totalUser'), DB::raw('MONTH(created_at) as months'))
            ->whereYear('created_at', Carbon::now()->year)
            ->groupBy('months')
//            ->groupBy('user_id')
            ->get();

        $allMonths = all_month();
        if (isset($monthlyUsers[0])) {
            foreach ($monthlyUsers as $usr) {
                $data['user'][$usr->months] = $usr->totalUser;
            }
        }
        $allUsers= [];
        foreach ($allMonths as $month) {
            $allUsers[] =  isset($data['user'][$month]) ? (int)$data['user'][$month] : 0;
        }
        $data['monthly_user'] = $allUsers;

        $monthlyQuestions = Question::select(DB::raw('count(id) as totalQs'), DB::raw('MONTH(created_at) as months'))
            ->where('status', STATUS_ACTIVE)
            ->whereYear('created_at', Carbon::now()->year)
            ->groupBy('months')
            ->get();
        $allMonth = all_months();
        if (isset($monthlyQuestions[0])) {
            foreach ($monthlyQuestions as $mQs) {
                $data['qs'][$mQs->months] = $mQs->totalQs;
            }
        }
        $allQuestions= [];
        foreach ($allMonth as $month) {
            $allQuestions[] =  isset($data['qs'][$month]) ? (int)$data['qs'][$month] : 0;
        }
        $data['all_questions'] = $allQuestions;

        return view('admin.dashboard', $data);
    }

    /*
     * leaderBoard
     *
     * Leader board who have attend the quiz
     * And show their score and ranking
     *
     *
     *
     */
    public function leaderBoard()
    {
        $data['pageTitle'] = __('Leader Board');
        $data['menu'] = 'leaderboard';

        $get_adminSetting =  AdminSetting::where(['slug' => 'weekly_points_reset_date'])->first();

        info("admin setting ====".json_encode($get_adminSetting));

        $week_no = $get_adminSetting->value;
        $start_date = Carbon::parse($week_no);
        $end_date = Carbon::parse($week_no);
        $start_date->endOfDay();
        $end_date->subDays(7)->startOfDay();

        info('start date ============='.$start_date);
        info('end==================='.$end_date);
        $leaders = UserAnswer::select(
            DB::raw('SUM(point) as score, user_id'))
            ->groupBy('user_id')
            ->wherebetween('created_at', [$end_date,$start_date])
            ->orderBy('score', 'DESC')
            ->get();

        $get_user_ids = User::where('role',2)->pluck('id');

        $c = collect();
        foreach ( $get_user_ids as $single )
        {

            $local = new UserAnswer();
            $local->user_id = $single;
            $local->score = 0;
            if( $leaders->isNotEmpty())
            {
                foreach ($leaders as $leader)
                {
                    if( $leader->user_id == $single )
                    {
                        $local->score = $leader->score;
                        break;
                    }
                }
            }

            $c->push($local);
        }

        $sorted = $c->sortByDesc('score');


        $this->CronTaskFunction();

        $data['leaders'] = $sorted;
        return view('admin.leaderboard', $data);
    }

    /*
     * qsSearch
     *
     * Search the question in any page
     *
     *
     *
     *
     */

    public function qsSearch(Request $request)
    {
        $data['pageTitle'] = __('Search Result');
        $categories = Category::where('status',1)
            ->where('name','LIKE','%'.$request->item.'%')
            ->get();
        $questions = Question::where('status',1)
            ->where('title','LIKE','%'.$request->item.'%')
            ->get();
        $users = User::where('active_status',1)
            ->where('name','LIKE','%'.$request->item.'%')
            ->get();

        $data['users'] = $users;
        $data['questions'] = $questions;
        $data['categories'] = $categories;

        return view('admin.search-item', $data);
    }

    public  function CronTaskFunction(){

        info("Dashboard Leader board");

        $get_adminSetting =  AdminSetting::where(['slug' => 'weekly_points_reset'])->first();

        $week_no = $get_adminSetting->value;

        $start_week = now()->startOfWeek();

//        info("start of year ".$start_week);

        for ($i = 1; $i <= 7; $i++) {

            if( $week_no == $i )
            {
                $get_admin_date =  AdminSetting::where(['slug' => 'weekly_points_reset_date'])->first();
                $week_date = $get_admin_date->value;
//                $week_date = $start_week;

                info("week Date ================ ".$week_date);
                info("current  Date ==============". now()->toDateString());

                if(    $week_date  < now()->toDateString() )
                {
                    info("Cron runing date && time now");

                    $start_date = Carbon::parse($week_date);

                    $end_date = Carbon::parse($week_date);

                    $end_date = $end_date->subdays(7);


                    info('start date ===============' . $start_date->endOfDay());
                    info('end date===========' . $end_date->startOfDay());
//

                    $leaders = UserAnswer::select(
                        DB::raw('SUM(point) as score, user_id'))
                        ->wherebetween('created_at',[$end_date->startOfDay(), $start_date->endOfDay()])
                        ->groupBy('user_id')
                        ->orderBy('score', 'DESC')
                        ->having('score' ,'>', 0)
                        ->get();

                    if( $leaders->isNotEmpty())
                    {
                        foreach ($leaders as $leader)
                        {
                            info("user_id============".$leader->user_id.'=====score==========='.$leader->score);
                            $prize = new Prize();
                            $prize->points = $leader->score;
                            $prize->user_id = $leader->user_id;
                            $prize->reset_points_date = $start_date->toDateString();
                            $prize->month_date = now()->toDateString();
                            $prize->save();
                        }
                    }

                    UserAnswer::wherebetween('created_at',[$end_date->startOfDay(), $start_date->endOfDay()])
//                    UserAnswer::where('created_at', '>=', $end_date->toDateString() . ' 00:00:00')->where('created_at', '<=', $start_date->toDateString() . ' 23:59:59')
                        ->update(['point' => 0]);
//                        ->get();

//                    info("get data -----------".json_encode($leaders));

                    $next_date = $start_date->addDays(7);
                    info('start date after ' . $next_date);

                    AdminSetting::updateOrCreate(['slug' => 'weekly_points_reset_date'],['value' =>$next_date->toDateString()]);
                }
                else
                {
                    info("Cron Not runing date ");
                }

            }
            $start_week->addDay();
        }
    }

}
