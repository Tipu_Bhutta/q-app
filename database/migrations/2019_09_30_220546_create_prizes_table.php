<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prizes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->default(0);
            $table->integer('first_prize_day')->default(0)->nullable();
            $table->integer('second_prize_day')->default(0)->nullable();
            $table->integer('third_prize_day')->default(0)->nullable();
            $table->integer('first_prize_week')->default(0)->nullable();
            $table->integer('second_prize_week')->default(0)->nullable();
            $table->integer('third_prize_week')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prizes');
    }
}
