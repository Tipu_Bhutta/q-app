@extends('layout.master')
@section('title') @if (isset($pageTitle)) {{ $pageTitle }} @endif @endsection

@section('left-sidebar')
    @include('layout.include.sidebar')
@endsection

@section('header')
    @include('layout.include.header')
@endsection

@section('main-body')
    <!-- Start page title -->
    <div class="qz-page-title">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-between">
                        <h2>{{__('Quiz')}}</h2>
                        <div class="d-flex align-items-center">
                            
                           
                            </a>
                            
                            <span class="sidebarToggler ml-4">
                                <i class="fa fa-bars d-lg-none d-block"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End page title -->
    @include('layout.message')
    <!-- Start content area  -->
    <div class="qz-content-area">
        <div class="card">
            <div class="card-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="">
                                <table id="qz-question-table" class="table category-table table-bordered  text-center mb-0">
                                    <thead>
                                    <tr>
                                       
                                        <th>{{__('Limit Per Day')}}</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($items))
                                        @php ($sl = 1)
                                        @foreach($items as $item)
                                            <tr>
                                                
                                                <td>{{ $item->questionlim }}</td>
                                                
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                                {{ Form::open(['route' => 'qlimSave']) }}
                                <label style="padding-right:20px;">Limit
</label><input type="number" name="lim">
                                <button type="submit" class="btn btn-primary btn-block add-category-btn mt-4">
                                            Update
                                        </button>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End content area  -->
@endsection

@section('script')
@endsection