@extends('layout.master')
@section('title') @if (isset($pageTitle)) {{ $pageTitle }} @endif @endsection

@section('left-sidebar')
    @include('layout.include.sidebar')
@endsection

@section('header')
    @include('layout.include.header')
@endsection

@section('main-body')
    <!-- Start page title -->
    <div class="qz-page-title">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-between align-items-center">
                        <h2>Weekly Points Reset</h2>
                        <span class="sidebarToggler">
                            <i class="fa fa-bars d-lg-none d-block"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End page title -->
    @include('layout.message')
    <!-- Start content area  -->
    <div class="qz-content-area">
        <div class="card add-category">
            <div class="card-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            {{ Form::open(['route' => 'saveSettings', 'files' => 'true']) }}
                                <div class="col-sm-12 col-lg-4 offset-lg-4">
                                    <div class="form-group">
                                        <label>Choose Day</label>
                                        <div class="qz-question-category">
                                            <select name="weekly_points_reset" class="form-control">
                                                <option @if(isset($adm_setting['weekly_points_reset']) && $adm_setting['weekly_points_reset']== 1) selected @endif value="1">Monday</option>
                                                <option @if(isset($adm_setting['weekly_points_reset']) && $adm_setting['weekly_points_reset']== 2) selected @endif  value="2">Tuesday</option>
                                                <option @if(isset($adm_setting['weekly_points_reset']) && $adm_setting['weekly_points_reset']== 3) selected @endif value="3">Wednesday</option>
                                                <option @if(isset($adm_setting['weekly_points_reset']) && $adm_setting['weekly_points_reset']== 4) selected @endif value="4">Thursday</option>
                                                <option @if(isset($adm_setting['weekly_points_reset']) && $adm_setting['weekly_points_reset']== 5) selected @endif value="5">Friday</option>
                                                <option @if(isset($adm_setting['weekly_points_reset']) && $adm_setting['weekly_points_reset']== 6) selected @endif value="6">Saturday</option>
                                                <option @if(isset($adm_setting['weekly_points_reset']) && $adm_setting['weekly_points_reset']== 7) selected @endif value="7">Sunday</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-lg-4 offset-lg-4">
                                    <button type="submit" class="btn btn-primary btn-block add-category-btn mt-4">Save</button>
                                </div>
                            {{ Form::close() }}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End content area  -->
@endsection

@section('script')
@endsection
