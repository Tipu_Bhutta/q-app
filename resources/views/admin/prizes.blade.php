@extends('layout.master')
@section('title') @if (isset($pageTitle)) {{ $pageTitle }} @endif @endsection

@section('left-sidebar')
    @include('layout.include.sidebar')
@endsection

@section('header')
    @include('layout.include.header')
@endsection

@section('main-body')
    <!-- Start page title -->
    <div class="qz-page-title">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-between align-items-center">
                        <h2>Prizes</h2>
                        <span class="sidebarToggler">
                            <i class="fa fa-bars d-lg-none d-block"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End page title -->
    @include('layout.message')
    <!-- Start content area  -->
    <div class="qz-content-area">
        <div class="card add-category">
            <div class="card-body">
                <div class="container-fluid">
                    {{ Form::open(['route' => 'saveSettings', 'files' => 'true']) }}
                    <div class="row">
                        <div class="col-sm-12 col-lg-3">
                            <a target="_blank" href="https://playingandwinning.com/Winners/Winners.htm" class="btn btn-primary btn-block add-category-btn mt-4 prize-btn">Prizes</a>
                        </div>
                        <div class="col-sm-12 col-lg-9"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-4">
                             <button type="button" class="btn btn-primary btn-block add-category-btn mt-4">Daily</button>
                        </div>
                    
                        <div class="col-sm-12 col-lg-4">
                            <div class="main-block align-items-center">
                                <div class="row">
                                    <div class="prize-block">
                                        <p>1st</p>
                                    </div>
                                     <div class="prize-block">
                                         <p>{{ (isset($adm_setting['first_prize_day']) )? $adm_setting['first_prize_day']:0 }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="prize-block">
                                        <p>2nd</p>
                                    </div>
                                     <div class="prize-block">
                                         <p>{{ (isset($adm_setting['second_prize_day']) )? $adm_setting['second_prize_day']:0 }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="prize-block">
                                        <p>3rd</p>
                                    </div>
                                     <div class="prize-block">
                                         <p>{{ (isset($adm_setting['third_prize_day']) )? $adm_setting['third_prize_day']:0 }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="prize-input">
                                <div class="form-group">
                                    <label></label>
                                    <input type="text" name="first_prize_day" value ="" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label></label>
                                    <input type="text" name="second_prize_day" value ="" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label></label>
                                    <input type="text" name="third_prize_day" value ="" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-4">
                             <button type="button" class="btn btn-primary btn-block add-category-btn mt-4">Weekly</button>
                        </div>
                    
                        <div class="col-sm-12 col-lg-4">
                            <div class="main-block align-items-center">
                                <div class="row">
                                    <div class="prize-block">
                                        <p>1st</p>
                                    </div>
                                     <div class="prize-block">
                                        <p>{{ isset($adm_setting['first_prize_week'])  ? $adm_setting['first_prize_week']:0 }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="prize-block">
                                        <p>2nd</p>
                                    </div>
                                     <div class="prize-block">
                                         <p>{{ isset($adm_setting['second_prize_week']) ? $adm_setting['second_prize_week']:0 }}
                                             </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="prize-block">
                                        <p>3rd</p>
                                    </div>
                                     <div class="prize-block">
                                        <p>{{ isset($adm_setting['third_prize_week']) ? $adm_setting['third_prize_week']:0 }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="prize-input">
                                <div class="form-group">
                                    <label></label>
                                    <input type="text" name="first_prize_week" value ="" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label></label>
                                    <input type="text" name="second_prize_week" value ="" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label></label>
                                    <input type="text" name="third_prize_week" value ="" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-3 offset-9">
                            <button type="submit" class="btn btn-primary btn-block add-category-btn mt-4">Save</button>
                        </div>
                        <!-- <div class="col-lg-9"></div> -->
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- End content area  -->
@endsection

@section('script')
@endsection
