<!-- Start sidebar -->
<div class="qz-sidebar">

    <div class="qz-logo">
        <a href="{{ route('adminDashboardView') }}">
            <img @if(!empty(allsetting('logo'))) src ="{{ asset(path_image().allsetting('logo')) }}"
                 @else src="{{asset('assets/images/logo.png')}}" @endif alt="" class="img-fluid">
        </a>
    </div>

    <nav>

        <ul id="metismenu">
            <li class="@if(isset($menu) && $menu == 'dashboard') qz-active @endif"><a href="{{ route('adminDashboardView') }}"><span class="flaticon-dashboard"></span>{{__('Dashboard')}} </a></li>
            <li class="@if(isset($menu) && $menu == 'category') qz-active @endif"><a href="{{ route('qsCategoryList') }}"><span class="flaticon-menu"></span>{{__('Category')}} </a></li>
            <li class="@if(isset($menu) && $menu == 'question') qz-active @endif"><a href="{{ route('questionList') }}"><span class="flaticon-info"></span>{{__('Question')}} </a></li>
            <li class="@if(isset($menu) && $menu == 'leaderboard') qz-active @endif"><a href="{{ route('leaderBoard') }}"><span class="flaticon-statistics"></span>{{__('Leaderboard')}} </a></li>
            <li class="@if(isset($menu) && $menu == 'userlist') qz-active @endif"><a class="userlist-image" href="{{ route('userList') }}"><img src={{asset('assets/images/friend.jpg')}}>{{__('User Management')}} </a></li>
            <li class="@if(isset($menu) && $menu == 'profile') qz-active @endif"><a href="{{ route('userProfile') }}"><span class="flaticon-user"></span>{{__('Profile')}} </a></li>
            <li class="@if(isset($menu) && $menu == 'setting') qz-active @endif"><a href="{{ route('generalSetting') }}"><span class="flaticon-settings-work-tool"></span>{{__('Settings')}} </a></li>
            <li><a href="{{ route('qlimList') }}"><span class="flaticon-settings-work-tool"></span>Quiz Limit</a></li>
            <li class="@if(isset($menu) && $menu == 'weekly-points') qz-active @endif"><a href="{{ route('weekly-points') }}"><span class="flaticon-settings-work-tool"></span>{{__('Weekly Points Reset')}} </a></li>
            <li class="@if(isset($menu) && $menu == 'prizes') qz-active @endif"><a href="{{ route('prizes') }}"><span class="flaticon-settings-work-tool"></span>{{__('Prizes')}} </a></li>
        </ul>

    </nav>

</div>
<!-- End sidebar -->
