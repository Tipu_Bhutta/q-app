<?php

Route::group(['middleware' => ['auth','admin','lang']], function() {
    Route::get('/', 'Admin\DashboardController@adminDashboardView')->name('adminDashboardView');
    Route::get('/search', 'Admin\DashboardController@qsSearch')->name('qsSearch');

    //profile setting
    Route::get('/profile','Admin\ProfileController@userProfile')->name('userProfile');
    Route::get('/password-change','Admin\ProfileController@passwordChange')->name('passwordChange');
    Route::post('/update-profile','Admin\ProfileController@updateProfile')->name('updateProfile');
    Route::post('/change-password','Admin\ProfileController@changePassword')->name('changePassword');

    //leaderboard
    Route::get('/leader-board', 'Admin\DashboardController@leaderBoard')->name('leaderBoard');

    // Setting
    Route::get('general-setting', 'Admin\SettingController@generalSetting')->name('generalSetting');
    Route::get('weekly-points', 'Admin\SettingController@generalSettingForWeekly')->name('weekly-points');
    Route::get('prizes', 'Admin\SettingController@prizes')->name('prizes');

    Route::post('save-setting', 'Admin\SettingController@saveSettings')->name('saveSettings');

    //User Management
    Route::get('user-list', 'Admin\UserController@userList')->name('userList');
    Route::get('add-user', 'Admin\UserController@addUser')->name('addUser');
    Route::get('user-details/{id}', 'Admin\UserController@userDetails')->name('userDetails');
    Route::get('user-make-admin/{id}', 'Admin\UserController@userMakeAdmin')->name('userMakeAdmin');
    Route::get('user-make-user/{id}', 'Admin\UserController@userMakeUser')->name('userMakeUser');
    Route::get('user-edit/{id}', 'Admin\UserController@editUser')->name('editUser');
    Route::get('user-delete/{id}', 'Admin\UserController@userDelete')->name('userDelete');
    Route::get('user-active/{id}', 'Admin\UserController@userActivate')->name('userActivate');
    Route::post('user-add-process', 'Admin\UserController@userAddProcess')->name('userAddProcess');
    Route::post('user-update-process', 'Admin\UserController@userUpdateProcess')->name('userUpdateProcess');

    //Question Category
    Route::get('question-category-list', 'Admin\CategoryController@qsCategoryList')->name('qsCategoryList');
    Route::get('question-sub-category-list-{id}', 'Admin\CategoryController@qsSubCategoryList')->name('qsSubCategoryList');
    Route::get('question-category-create', 'Admin\CategoryController@qsCategoryCreate')->name('qsCategoryCreate');
    Route::get('question-sub-category-create/{id}', 'Admin\CategoryController@qsSubCategoryCreate')->name('qsSubCategoryCreate');
    Route::post('question-category-save', 'Admin\CategoryController@qsCategorySave')->name('qsCategorySave');
    Route::get('question-category-edit/{id}', 'Admin\CategoryController@qsCategoryEdit')->name('qsCategoryEdit');
    Route::get('question-category-delete/{id}', 'Admin\CategoryController@qsCategoryDelete')->name('qsCategoryDelete');
    Route::get('question-category-activate/{id}', 'Admin\CategoryController@qsCategoryActivate')->name('qsCategoryActivate');
    Route::get('question-category-deactivate/{id}', 'Admin\CategoryController@qsCategoryDeactivate')->name('qsCategoryDeactivate');

    //Question
    Route::get('question-list', 'Admin\QuestionController@questionList')->name('questionList');
    Route::get('category-question-list/{id}', 'Admin\QuestionController@categoryQuestionList')->name('categoryQuestionList');
    Route::get('question-create', 'Admin\QuestionController@questionCreate')->name('questionCreate');
    Route::post('question-save', 'Admin\QuestionController@questionSave')->name('questionSave');
    Route::get('question-edit/{id}', 'Admin\QuestionController@questionEdit')->name('questionEdit');
    Route::get('question-delete/{id}', 'Admin\QuestionController@questionDelete')->name('questionDelete');
    Route::get('question-activate/{id}', 'Admin\QuestionController@questionActivate')->name('questionActivate');
    Route::get('question-deactivate/{id}', 'Admin\QuestionController@questionDectivate')->name('questionDectivate');
    Route::get('excel-upload', 'Admin\QuestionController@qsExcelUpload')->name('qsExcelUpload');
    Route::post('excel-upload-process', 'Admin\QuestionController@qsExcelUploadProcess')->name('qsExcelUploadProcess');
    
    Route::get('qlimlist', 'Admin\QuestionController@qlimList')->name('qlimList');
    Route::post('lim-save', 'Admin\QuestionController@qlimSave')->name('qlimSave');





});